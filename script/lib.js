function invertB(arg){
    return !arg;
}

function slides(obj,direction){
    // NOTE FOR FUTURE CHANGE
    // obj.parent.parent must be a var to change
    
    //direction prev,next
    _iter=obj.parent().parent().attr('iter'); //the object needs attr iter to start wit.maay change
    if(direction=='next')_iter++;
    if(direction=='prev')_iter--;
    obj.parent().parent().attr('iter',_iter);//sets attribute
    obj.parent().parent().find('img').hide()
    currImage=obj.parent().parent().find('img:nth-child('+_iter+')'); //image wo work with
    if(!currImage.attr('src')){ //if image does not exist
        if(direction == 'next'){
            obj.parent().parent().attr('iter',2);
            currImage=obj.parent().parent().find('img:nth-child(2)');
        }
        if(direction == 'prev'){
	    //count how many images are in div.
	    //to set an iterator correctly
            _cnt=0;
            $(obj.parent().parent().find('img')).each(function(){
                _cnt++;
            });
            obj.parent().parent().attr('iter',_cnt+2);
            currImage=obj.parent().parent().find('img:nth-child('+_cnt+')');
        }
    }//if image does not exist
    currImage.show();
}
