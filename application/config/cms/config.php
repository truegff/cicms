<?php

$config['config']['general'] = array( 
				     'uploads_dir' => '../uploads', // relative to index.php
				      );

$config['config']['tables'] = array(
				    'events' => array('label' => 'ივენთი', 'icon' => 'glyphicon glyphicon-tower'),
				    'users' => array('label' => 'მომხმარებლები', 'icon' => 'glyphicon glyphicon-tower'),
				    'eventData' => array('label' => 'ივენთის მონაცემები', 'icon' => 'glyphicon glyphicon-tower'),
				    'userEvents' => array('label' => 'მომხმარებლის ივენთები', 'icon' => 'glyphicon glyphicon-tower'),
				    );



// documentation config.org
$config['config']['events'] = array(
				    'id' => array('type' => 'hidden', 'label' => 'id', 'visible' => true),
				    'name' => array('type' => 'text', 'label' => 'სახელი', 'visible' => true),
				    'desc' => array('type' => 'textarea', 'label' => 'აღწერა', 'visible' => false),
				    'himage' => array('type' => 'file', 'label' => 'მთავარი სურათი', 'visible' => false),
				    'mimage' => array('type' => 'file', 'label' => 'შიდა სურათი', 'visible' => false)
				    );
$config['config']['users'] = array(
				   'id' => array('type' => 'hidden', 'label' => 'id', 'visible' => true),
				   'fbid' => array('type' => 'text', 'label' => 'fb id', 'visible' => true),
				   'name' => array('type' => 'text', 'label' => 'სახელი', 'visible' => true),
				   'lastname' => array('type' => 'text', 'label' => 'გვარი', 'visible' => true),
				   'phone' => array('type' => 'text', 'label' => 'ტელეფონი', 'visible' => true),
				   'mail' => array('type' => 'text', 'label' => 'სქესი', 'visible' => true),
				   'age' => array('type' => 'text', 'label' => 'ასაკი', 'visible' => true),

				   );
$config['config']['eventData'] = array(
				     'id' => array('type' => 'hidden', 'label' => 'id', 'visible' => true),
				     'eventid' => array('type' => 'dropdown', 'relation' => 'events', 'relation_column' => 'name' ,'visible' => true, 'label' => 'ივენთი'),
				     'adress' => array('type' => 'text', 'label' => 'მისამართი ქართ', 'visible' => true),
				     'date' => array('type' => 'date', 'label' => 'თარიღი', 'visible' => true),
				     'time' => array('type' => 'text', 'label' => 'დრო', 'visible' => true),
				     'maxplaces' => array('type' => 'text', 'label' => 'მაქსიმალური ადგილების რაოდ', 'visible' => true),
				     'reserved' => array('type' => 'text', 'label' => 'დაჯავშნული', 'visible' => true),
				     );

$config['config']['userEvents'] = array(
					      'id' => array('type' => 'hidden', 'visible' => true, 'label' => 'id'),
					      'userid' => array('type' => 'dropdown', 'relation' => 'users' , 'relation_column' => 'name' , 'visible' => true, 'label' => 'მომხმარებელი'),
					      'eventid' => array('type' => 'dropdown', 'relation' => 'eventData', 'relation_column' => 'id' , 'visible' => true, 'label' => 'ივენთი'),
					      );

